local paginaPianeta= require ("planetPage")
local widget=require "widget"
local scene=require "addSceneWidgets"
local data=require "data"
local new_pianetaPage=require "newPlanetPage"
local zoom_util=require "zoomOutInMain"
local db_interface=require "databaseInterface"
local sqlite3 = require( "sqlite3" )
local utility = require("utility")
local os = require("os")
local myPlanetPage = require("myPlanetPage")
-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------


---------------------------------------


 
-- Open "data.db". If the file doesn't exist, it will be created
-- local path = system.pathForFile( "data.db", system.DocumentsDirectory )
-- local db = sqlite3.open( path )   
 
-- -- Handle the "applicationExit" event to close the database
-- local function onSystemEvent( event )
--     if ( event.type == "applicationExit" ) then             
--         db:close()
--     end
-- end
  
-- -- Set up the table if it doesn't exist
-- local tablesetup = [[CREATE TABLE IF NOT EXISTS test (id INTEGER PRIMARY KEY, content, content2);]]
-- print( tablesetup )
-- db:exec( tablesetup )
  
-- -- Add rows with an auto index in 'id'. You don't need to specify a set of values because we're populating all of them.
-- local testvalue = {}
-- testvalue[1] = "Hello"
-- testvalue[2] = "World"
-- testvalue[3] = "Lua"
-- local tablefill = [[INSERT INTO test VALUES (NULL, ']]..testvalue[1]..[[',']]..testvalue[2]..[['); ]]
-- local tablefill2 = [[INSERT INTO test VALUES (NULL, ']]..testvalue[2]..[[',']]..testvalue[1]..[['); ]]
-- local tablefill3 = [[INSERT INTO test VALUES (NULL, ']]..testvalue[1]..[[',']]..testvalue[3]..[['); ]]
-- db:exec( tablefill )
-- db:exec( tablefill2 )
-- db:exec( tablefill3 )
  
-- -- Print the SQLite version
-- print( "SQLite version " .. sqlite3.version() )
  
-- -- Print the table contents
-- for row in db:nrows("SELECT * FROM test") do
--     local text = row.content .. " " .. row.content2
--     print(text)
-- end
 
-- -- Setup the event listener to catch "applicationExit"
-- Runtime:addEventListener( "system", onSystemEvent )



-- ---------------------------------------



-- --stmt = db:prepare[[ INSERT INTO test VALUES (:key, :value) ]]

-- --stmt:bind{  key = 1,  value = "Hello World"    }:exec()
-- --stmt:bind{  key = 2,  value = "Hello Lua"      }:exec()
-- --stmt:bind{  key = 3,  value = "Hello Sqlite3"  }:exec()


-- --for row in db:rows("SELECT * FROM test") do   nequesto
--  --- print(row.id, row.content)
--  print("SELECT * FROM test")

--  print(sqlite3.version())
-- for row in db:rows("SELECT * FROM test") do
--   print(row.id, row.content)
-- end

-- --end

-- db:close()

-------------------------------------------


local function update_coordinates(event)
	for index,pianeta in pairs(pianeti) do
		pianeta.angolo=pianeta.angolo+10/(pianeta.raggio*pianeta.raggio*scale*scale)
		local xNuovo = display.contentCenterX + math.cos(pianeta.angolo)*pianeta.raggio*scale;
	  	local yNuovo = display.contentCenterY + math.sin(pianeta.angolo)*pianeta.raggio*scale;

		if not pianeta.isFocused then
			transition.to( pianeta, {time=0,x=xNuovo,y=yNuovo} )
		end
	end
end



function checkMultipleTouch(event)
	if event.phase=="began" then
		touches[event.id]={};
		touches.count=touches.count+1;
	end

	if event.phase=="moved" then
		touches[event.id].x = event.x;
		touches[event.id].y = event.y;
		print("x tocco: "..touches[event.id].x);
		print("y tocco: "..touches[event.id].y);
	end

	if event.phase=="ended" or event.phase=="canceled" then
		touches[event.id]=nil;
		touches.count=touches.count-1;
	end

	print("n tocchi: ".. touches.count);
end

function checkPinchToZoom(event)
	if touches.count == 2 then
		
	end
end

--*******************  INIZIO PROGRAMMA  **************
touches={}
touches.count=0;

system.activate( "multitouch" )

myPlanet=display.newCircle( display.contentCenterX, display.contentCenterY, 25 )

add_pianeta(1,180,120)
add_pianeta(2,0,70)
add_pianeta(3,90,170,15)
add_pianeta(4,100,220)
add_pianeta(5,130,35,5)
drawAddButton()

init()
doTest()
create_new_world(7, 'ciao', nil, os.date( "*t" ), tipo_mondo.singolo, 'niente')
close_db()
testDate()


-- zoom(pianeti,2,easing.inOutCubic)
-- zoom(orbite,2,easing.inOutCubic)


--****** EVENTS **********
myPlanet:addEventListener("tap",openMyPlanet)
for index,pianeta in pairs( pianeti ) do
	pianeta:addEventListener("tap",open_world)
end
--myPlanet:addEventListener("tap",open_world)
Runtime:addEventListener( "enterFrame", update_coordinates)

background:addEventListener("touch",checkMultipleTouch);
background:addEventListener("touch",checkPinchToZoom);

