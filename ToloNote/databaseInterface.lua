local db

function init()
	local path=system.pathForFile( "data.db", system.DocumentsDirectory )

	print("using "..path)
	db = sqlite3.open(path)

	if db == nil then
		print("database load error")
		return
	end

	create_table_ifnotExist();

end

function create_new_world(id, nome, data_fine, data_creazione, tipo, descr)

	local data_in = fromDateToString(data_creazione)

	local data_out
	if data_fine == nil then
		data_out = ""
	else
		data_out = fromDateToString(data_fine)
	end
	

	local query = [[INSERT INTO mondo (mondo_id, mondo_nome, mondo_data_fine, mondo_data_creazione, mondo_categoria, mondo_descrizione)
			VALUES  (]] .. id .. [[, ']] .. nome .. [[', ']] .. data_out .. [[', ']] .. data_in .. [[', ']] .. tipo .. [[', ']] .. descr .. [[');]]
	db:exec(query)
	print("creato mondo".. id)
end

function update_world(id, new_params)

end

function delete_world(id)

end


function create_table_ifnotExist()
	db:exec[[CREATE TABLE IF NOT EXISTS mondo (
				mondo_id integer PRIMARY KEY,
				mondo_nome text NOT NULL,
				mondo_data_fine text,
				mondo_data_creazione text NOT NULL,
				mondo_categoria text NOT NULL,
				mondo_descrizione text
	);]]

	db:exec[[CREATE TABLE IF NOT EXISTS orario (
				orario_id integer PRIMARY KEY,
				orario_mondo_id integer NOT NULL,
				orario_giorno_inizio numeric NOT NULL,
				orario_ora_inizio text NOT NULL,
				orario_ora_fine text NOT NULL, 
				orari_allarme_default boolean NOT NULL,
				orari_partecipazione_default boolean NOT NULL,
				FOREIGN KEY (orario_mondo_id) REFERENCES mondo (mondo_id)
				ON DELETE CASCADE ON UPDATE NO ACTION 
	);]]

	db:exec[[CREATE TABLE IF NOT EXISTS info (
				info_id integer NOT NULL,
				info_orario_id integer NOT NULL,
				info_descrizione text,
				info_data text NOT NULL,
				info_modifica_allarme text NOT NULL, 
				info_modifica_partecipazione text NOT NULL,
				PRIMARY KEY (info_orario_id , info_id),
				FOREIGN KEY (info_orario_id ) REFERENCES orario (orario_id)
				ON DELETE CASCADE ON UPDATE NO ACTION
	);]]

end

function close_db()
	db:close()
end

--FUNZIONI PER GESTIONE COSE



function doTest()
	db:exec[[INSERT INTO mondo (mondo_id, mondo_nome, mondo_data_fine, mondo_data_creazione, mondo_categoria, mondo_descrizione)
			VALUES  (1, 'Parkour', '01-12-17', '15-11-17', 'Ripetitivo', 'Sport di Daniele');]]
	db:exec[[INSERT INTO orario (orario_id, orario_mondo_id, orario_giorno_inizio, orario_ora_inizio, orario_ora_fine, orari_allarme_default, orari_partecipazione_default)
			VALUES  (1, 1, 'martedi', '20.30', '22.00', 'NO', 'SI');]]
	db:exec[[INSERT INTO orario (orario_id, orario_mondo_id, orario_giorno_inizio, orario_ora_inizio, orario_ora_fine, orari_allarme_default, orari_partecipazione_default)
			VALUES  (2, 1, 'giovedi', '20.30', '22.00', 'NO', 'SI');]]

	for row in db:nrows("SELECT * FROM mondo , orario WHERE mondo_id=orario_mondo_id ;") do
  		print(row.orario_giorno_inizio)
	end
	--print("valore: "..db:exec[[SELECT mondo_descrizione FROM mondo WHERE mondo_id='1']])
end