
--new planet page

local open_close_time=400

function goToNewPianeta()
	
end

function exitNewPianeta(event)

end

function add_pianeta(id,ang,rag,r)
	--aggiunge pianeta alla table pianeti
	local testPianeta=display.newCircle( 0,0 , r or 20 )
	testPianeta.id=id
	testPianeta.angolo=ang
	testPianeta.raggio=rag
	testPianeta.dimensione=r or 20
	testPianeta.isFocused=false
	--table.insert( pianeti, testPianeta )
	pianeti[id]=testPianeta
	--aggiunge l'orbita
	orbita=display.newCircle( display.contentCenterX, display.contentCenterY, rag )
	orbita.fill=nil
	orbita.stroke={ 255, 255, 255 ,0.4}
	orbita.strokeWidth=1.5
	orbite[id]=orbita
end

function removePianeta(id)
	pianeti[id]:removeSelf()
	pianeti[id]=nil
	orbite[id]:removeSelf()
	orbite[id]=nil
end