local widget=require "widget"

local table_view;

function openMyPlanet()
	zoom_(6, easing.inOutCubic , 1000)
	table_view = widget.newTableView( {x=display.contentCenterX, y=display.contentCenterY, width = 200, height = 200})
	table_view.backgroundColor = { 0.1, 0.8, 0.8 }
	--table_view.isVisible = false
	
	--transition.scaleBy(element,{xScale=0.01,yScale=0.01,time=0,transition=easing.linear})
	transition.fadeIn(table_view,{time = 50,transition=easing.inOutCubic})
end

function closeMyPlanet( )
	zoom_(6, easing.inOutCubic , 1000)
	table_view = widget.newTableView( {x=display.contentCenterX, y=display.contentCenterY, width = 200, height = 200})


	transition.fadeOut(table_view,{time = 50,transition=easing.inOutCubic})
end


function loadTest()

end