
local time_zoom = 50

--target is a table of displayObjects
--sc is a value of scale
--type is the type of animation
function zoom(target, sc, easing)
	print("zoomming")
	--must scale every object
	scale=sc
	for index,element in pairs(target) do
		
		transition.scaleTo(element,{xScale=sc,yScale=sc,time=time_zoom,transition=easing})
	end

	--must move every object
	-- for index,element in pairs(target) do

	-- 	transition.moveTo(element,{x=element.anchorX+(display.contentCenterX-element.anchorX)*scale,y=element.anchorY+(display.contentCenterY-element.anchorY)*scale,time=time_zoom,transition=easing})
	-- end
end

function zoom_with_time( target, sc, easing , time )
	print("zoomming")
	--must scale every object
	scale=sc
	for index,element in pairs(target) do
		
		transition.scaleTo(element,{xScale=sc,yScale=sc,time=time,transition=easing})
	end

	--must move every object
	-- for index,element in pairs(target) do

	-- 	transition.moveTo(element,{x=element.anchorX+(display.contentCenterX-element.anchorX)*scale,y=element.anchorY+(display.contentCenterY-element.anchorY)*scale,time=time_zoom,transition=easing})
	-- end
end



--zoom_level = 1
--local zoom_bg=1
function zoom_(scale, easing , time)
	-- if in_out == "in" then
	-- 	zoom_bg=zoom_bg (scale/5);
	-- else
	-- 	zoom_bg=zoom_bg-0.01;
	-- end
	
	--print(zoom_bg);
	zoom_with_time({background},scale/5,easing,time);
	--zoom_level=zoom_level+0.05;
	zoom_with_time(pianeti,scale,easing,time);
	zoom_with_time(orbite,scale,easing,time);
	zoom_with_time({myPlanet},scale,easing,time);
	
end