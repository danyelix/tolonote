local widget= require "widget"
local scene= require ("addSceneWidgets")
-- local data = require "data"

local function schermataPianeta()

end

local centerOfWorld={}

time_open_close_world=400

local function close_world(planet)
	transition.fadeOut( planet.container, {time=time_open_close_world/3} )
	local angolo=planet.angolo+(10/(planet.raggio*planet.raggio*scale*scale))*time_open_close_world/1000*display.fps --calcolando che sono 60 fps altrimenti non sarebbe 24 ma 12 calcolato come 0,4(tempo della animazione)*fps
	transition.to( planet,{x=display.contentCenterX + math.cos(angolo)*planet.raggio*scale,y=display.contentCenterY + math.sin(angolo)*planet.raggio*scale,transition=easing.inOutCubic,time=time_open_close_world,xScale=scale,yScale=scale,onComplete= (function() planet.isFocused=not planet.isFocused end)})
	background.fill.effect=nil
	bottone.isVisible=(false)
	planet:addEventListener("tap",open_world)
	display.getCurrentStage():setFocus(nil)

	for id,pianeta in pairs(pianeti) do
		if id~=planet.id then
			transition.fadeIn( pianeta, { time=time_open_close_world } )
		end
		transition.fadeIn( orbite[id], { time=time_open_close_world } )
	end
end

--basato sul raggio dei pianeti pari a 20
local function planet_screen(planet)
	--sfoca il resto
	background.fill.effect="filter.blurGaussian"
	background.fill.effect.horizontal.blurSize=2
	background.fill.effect.vertical.blurSize=2
	transition.to( background.fill.effect.horizontal, { time=time_open_close_world, blurSize=10 } )
	transition.to( background.fill.effect.vertical, { time=time_open_close_world, blurSize=10 } )
	

	for id,pianeta in pairs(pianeti) do
		if id~=planet.id then
			transition.fadeOut( pianeta, { time=time_open_close_world } )
		end
		transition.fadeOut( orbite[id], { time=time_open_close_world } )
	end


	-- local gruppoPianeta=display.newGroup()
	-- gruppoPianeta:insert(planet)
end

--*************** main function *********************

function open_world(event)
	local pianetaAperto=event.target

	--set center of the known world
	centerOfWorld.x=display.contentCenterX
	centerOfWorld.y=display.contentCenterY

	transition.to( pianetaAperto,{x=display.contentCenterX, y=display.contentCenterY,transition=easing.inOutCubic, time=time_open_close_world,xScale=160/pianetaAperto.dimensione or 8,yScale=160/pianetaAperto.dimensione or 8,onStart= function() planet_screen(pianetaAperto) end})

	bottone=widget.newButton( {x=display.contentCenterX,y=470,shape="roundedRect",fillColor={ default={ 1, 0.1, 0.5, 0.7 }, over={ 1, 0.1, 0.5, 0.9 } },cornerRadius=17,onRelease=function() close_world(pianetaAperto)end, labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } }} )
	bottone:setLabel("CHIUDI")
	pianetaAperto:removeEventListener("tap",open_world)
	pianetaAperto.isFocused=true
	
end
