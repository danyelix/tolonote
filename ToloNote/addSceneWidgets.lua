local widget=require "widget";

local background
function drawBG()
	background=display.newImageRect( "res/star-background-gif-8.gif" , 1000, 600 )
	background.x=display.contentCenterX;
	background.y=display.contentCenterY;
	return background
end

local bottone_add
local bottone_meno

function drawAddButton(color,size)
	bottone_add=widget.newButton( {x=280,y=480,shape="roundedRect",width=70,height=70,fillColor={ default=color or { 1, 0.1, 0.5, 0.7 }, over={ 1, 0.1, 0.5, 0.9 } },cornerRadius=35, fontSize=50,labelColor = { default={ 0.8,0.8 , 0.8 }, over={ 0, 0, 0, 0.5 } }} )
	bottone_add:setLabel("+")

	bottone_add:addEventListener("tap",zoom_ev);

	bottone_meno=widget.newButton( {x=60,y=480,shape="roundedRect",width=70,height=70,fillColor={ default=color or { 1, 0.1, 0.5, 0.7 }, over={ 1, 0.1, 0.5, 0.9 } },cornerRadius=35, fontSize=50,labelColor = { default={ 0.8,0.8 , 0.8 }, over={ 0, 0, 0, 0.5 } }} )
	bottone_meno:setLabel("-")

	bottone_meno:addEventListener("tap",zoom_ve);
end

zoom_level = 1
zoom_bg=1
function zoom_ev(event)
	zoom_bg=zoom_bg+0.01;
	print(zoom_bg);
	zoom({background},zoom_bg,easing.linear);
	zoom_level=zoom_level+0.05;
	zoom(pianeti,zoom_level,easing.linear);
	zoom(orbite,zoom_level,easing.linear);
	zoom({myPlanet},zoom_level,easing.linear);
	
end

function zoom_ve(event)
	--a 0.8 da problemi
	zoom_bg=zoom_bg-0.01;
	print(zoom_bg);
	zoom({background},zoom_bg,easing.linear);
	zoom_level=zoom_level-0.05;
	zoom(pianeti,zoom_level,easing.linear);
	zoom(orbite,zoom_level,easing.linear);
	zoom({myPlanet},zoom_level,easing.linear);

end