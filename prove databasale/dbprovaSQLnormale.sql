CREATE TABLE IF NOT EXISTS mondo (
mondo_id integer PRIMARY KEY,
mondo_nome varchar NOT NULL,
mondo_data_fine date, ---> conteggiata, assegnata o senza data
/* da decidere se scriverci solo la data di fine (sia conteggiata che assegnata) --->in caso di conteggiata ogni volta dovrebbe ricalcolare

oppure mettere una data in caso di assegnata e il conteggio in ore/giorni se da contare---> come riconoscerle?

nel caso di eventi singoli equivale alla data dell'evento eccezionale*/

mondo_data_creazione date NOT NULL,/*data per poter memorizzare anche eventi passati  

  --- not null?? (o di default per utente not null)*/

mondo_ripetizioni boolean, /* falso orario fisso, vero orario con ripetizioni, null evento eccezionale*/

/*� un mondo che descrive un'attivit� ripetuta (parkour) o una fissa (sma/palestra) o unica */

mondo_descrizione varchar);

CREATE TABLE IF NOT EXISTS orario (
orario_id integer PRIMARY KEY,
orario_mondo_id integer NOT NULL,
orario_giorno_inizio numeric NOT NULL,
/*orario_giorno_fine text lo sto gi� inserendo in orario di fine +1 per ogni giorno in pi� rispetto al giorno di inizio*/
orario_ora_inizio text NOT NULL, ---> not null? e per gli eventi unici? datetime
orario_ora_fine text NOT NULL,  ---> not null? e per gli eventi unici?  boh
orari_allarme_default boolean NOT NULL,
orari_partecipazione_default boolean NOT NULL,
FOREIGN KEY (orario_mondo_id) REFERENCES mondo (mondo_id)
ON DELETE CASCADE ON UPDATE NO ACTION 

/*negli eventi fissi (sma/palestra) si distingue l'orario fisso da quello selezionato dall'utente: la partecipazione e l'allarme di default ----null/negativi
se l'orario � fisso l'utente non parteciper� a tutto l'orario per cui si potr� pi� in alto gestire
l'orario specifico a cui si partecipa (gi� nel mondo c'� in data di fine un qualcosa che determina se quella categoria � fissa o no)*/

);

CREATE TABLE IF NOT EXISTS info ( ------> DA MIGLIORARE
info_id integer NOT NULL,
info_orario_id integer NOT NULL,
info_descrizione varchar,
info_data date NOT NULL,
info_modifiva_allarme boolean NOT NULL, 
info_modifiva_partecipazione boolean NOT NULL,
/*l'idea era avere 2 booleani che segnalassero un cambiamento sulla partecipazione/allarme di un singolo evento specifico
es. 28/11 l'evento alle 13*/
PRIMARY KEY (info_orario_id , info_id),
FOREIGN KEY (info_orario_id ) REFERENCES orario (orario_id)
ON DELETE CASCADE ON UPDATE NO ACTION

/*se elimini l'orario o il mondo non puoi vedere n�' eventi futuri n� passati*/

);


------------------------------------------------------------- INSERT --------------------------------------------------------------------

INSERT INTO mondo (mondo_id, mondo_nome, mondo_data_fine, mondo_data_creazione, mondo_categoria, mondo_descrizione)

VALUES  

		(1, Parkour, 01-12-17, 15-11-17, Ripetitivo, Sport_di_daniele),

		(2, Sma, nullo, 15-11-17, Fisso, Supermercato_sotto_casa),

		(3, Dentista, 17-11-17, 15-11-17, Singolo, Appuntamento_dal_dentista);

	       

INSERT INTO orario (orario_id, orario_mondo_id, orario_giorno_inizio, orario_ora_inizio, orario_ora_fine, orari_allarme_default, orari_partecipazione_default)

VALUES  (1, 1, martedi, 20.30, 22.00, NO, SI),

		(2, 1, giovedi, 20.30, 22.00, NO, SI),

		(3, 2, lunedi, 08.00, 20.00, NO, NO),

		(4, 1, sabato, 18.30, 20.00, NO, NO),

		(5, 2, martedi, 08.00, 20.00, NO, NO),

		(6, 2, mercoledi, 08.00, 20.00, NO, NO),

		(7, 2, giovedi, 08.00, 20.00, NO, NO),

		(8, 2, venerdi, 08.00, 20.00, NO, NO),

		(9, 3, nullo/venerdi, 16.00, 17.30, SI, NO);

		/*questo allarme quando dovrebbe suonare? tutti la mattina? e la notifica solo se partecipi? la notifica quando?*/
